#!/bin/bash
domain=$1;

if [ -z "$1" ]; then 
	
	echo "Wrong use of the script";
	echo "Usage: $0 <domain name>";
else 
	echo "Ottengo i name server per $domain e li scrivo in ns.txt"
	host -t ns $domain | cut -d " " -f 4 > ns.txt
	echo "provo a fare Zone Trasfer su ciascun name server trovato"
	echo "salvo il risultato in zonet.txt"
	touch zonet.txt
	for ns in $(cat ns.txt);
	do 
		dig axfr @$ns $domain >> zonet.txt;
	done
fi
