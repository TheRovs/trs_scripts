import sys
import dns.resolver
import dns.query
import dns.zone
        
if (len(sys.argv)-1) < 1 :
    print ('Wrong usage')
    print('Usage:' + sys.argv[0] + '<domain name>')
else :
    domain = str(sys.argv[1])
    print ('Getting name servers for ' + domain)
    nameservers = dns.resolver.query('megacorpone.com', 'NS')
    for ns in nameservers:
        print (ns.to_text())
        ips = dns.resolver.query(str(ns), 'A')
        for ip in ips:
            print(str(ip))
            try:
                zone = dns.zone.from_xfr(dns.query.xfr(str(ip), domain))
                names = list(zone.nodes.keys())
                names.sort()
                for n in names:
                    #print raw zones
                    print (zone[n].to_text(n))
            except Exception as e:
                print('Exception on IP: ' + str(ip))
                print(e)
