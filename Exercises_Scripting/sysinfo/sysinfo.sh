#!/bin/bash
echo "sto per mostrarti alcune informazioni di sistema"
echo "users:"
awk -F : '{print $1}' /etc/passwd > users.txt
echo -e "printed in users.txt"
echo "groups:"
getent group > groups.txt
echo -e "printed in groups.txt"
echo "these are suid/guid binaries"
#find / -type f \( -perm -4000 -o -perm -2000 \) -exec ls -lg {} \; 2>/dev/null
echo "****"
echo "connections:"
netstat --protocol={inet,inet6}
echo "****"
echo "processes running for each user:"
ps aux>ps_user.txt
echo "****"
echo "operating system, architecture:"
hostnamectl
echo " "
uname -a
echo "****"
echo "Mounts:"
mount -l > mounts.txt
echo "printed in mounts.txt"

